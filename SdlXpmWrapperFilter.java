package com.contentbloom.sdl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;

import com.sdl.web.preview.client.filter.ClientBinaryContentFilter;
import com.sdl.web.preview.client.filter.ClientPageContentFilter;

import com.contentbloom.spring.properties.ApplicationProperties;
import com.contentbloom.util.LoggerUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CompositeFilter;

/**
 * A filter that conditionally initialises the SDL XPM ClientBinaryContentFilter and ClientPageContentFilter based on environment.properties
 * This allows us to use the same WAR on multiple environments.  
 * 
 * @author Content Bloom
 */
@Component("SdlXpmWrapperFilter")
public class SdlXpmWrapperFilter implements Filter {
			
	@Autowired
	@Qualifier("applicationProperties")
	protected ApplicationProperties applicationProperties;
	
    static LoggerUtil logger = LoggerUtil.getInstance(SdlXpmWrapperFilter.class);

    private CompositeFilter compositeFilter;
    private String isXpmEnabled;

	public void init(FilterConfig config) throws ServletException {
		
		logger.debug("in SdlXpmWrapperFilter init()");

		//replace this with your own method of getting environment specific variables
		this.isXpmEnabled = applicationProperties.getMessage("is.xpm.enabled");

		if(("true").equalsIgnoreCase(this.isXpmEnabled))
		{
			this.compositeFilter = new CompositeFilter();
 
			List<Filter> filters = new ArrayList<>();
			filters.add(new ClientPageContentFilter());
			filters.add(new ClientBinaryContentFilter());
		 
			compositeFilter.setFilters(filters);
			
			compositeFilter.init(config);
		}
	}
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException 
	{
		logger.debug("in SdlXpmWrapperFilter doFilter()");
		if(("true").equalsIgnoreCase(this.isXpmEnabled))
		{		
			logger.debug("SDL XPM is enabled on this environment.");
		    this.compositeFilter.doFilter(req, resp, chain);
		}
		else {
			chain.doFilter(req, resp);
		}
	}
	
	@Override
	public void destroy() {
	    this.compositeFilter.destroy();
	}
}
